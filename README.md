# linux_module_random

## Cel projektu
Celem projektu było stworzenie prostego modułu systemu operacyjnego Linux, który pod względem funkcjonalnym ogranicza się do generacji wartości losowej w zdefiniowanym przez użytkownika zakresie. Program komunikuje się poprzez urządzenie, na którym użytkownik może wykonać operacje zapisu oraz odczytu aby sterować funkcjonalnością.

## Instrukcja użytkownika
**Wczytanie modułu**

```
sudo insmod my_rand.ko
```
**Usuwanie modułu**
```
sudo rmmod my_rand 
```
**Odczyt wartości**
```
sudo cat /dev/my_random 
```
**Zmiana zakresu generowanej wartości**
```
sudo echo "M200" > /dev/my_random
sudo echo "m0" > /dev/my_random
```

## Kompilacja
```
make
```

## Pliki
Projekt składa się z następujących plików źródłowych:
- my_rand.c - kod modułu w języku C, pozwala na zmianę funkcjonalności i rozszerzenie modułu
- my_rand.ko - skompilowany moduł dla architektury x86 (dla wersji jądra 5.15.0-56-generic)
- Makefile - plik pozwalający na wygodne zbudowanie modułu

