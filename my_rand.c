#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/random.h>

MODULE_LICENSE("GPL");

/* Variables */
typedef struct rand {
	int max;
	int min;
} rand_t;

rand_t *rand_data;

/* For device */
static dev_t my_device_nr;
static struct class *my_class;
static struct cdev my_device;

#define DRIVER_NAME "my_random"
#define DRIVER_CLASS "driver_class"

/*
 * @brief Read   kernel space -> user space
 */
static ssize_t driver_read(struct file *File, char *user_buffer, size_t size,
			   loff_t *offs)
{
	ssize_t to_copy, not_copied, copied;
	int random_value;
	size_t data_size = 0;
	char rand_buffer[128] = "";

	if (*offs == 0) {
		if (rand_data->min >= rand_data->max) {
			printk(KERN_ERR "error - change min/max value \n");
			return 0;
		}

		get_random_bytes(&random_value, sizeof(random_value));
		random_value =
			abs(random_value) % (rand_data->max - rand_data->min) +
			rand_data->min;
		data_size = sprintf(rand_buffer, "%d", random_value);
	}

	to_copy = min(size, data_size);
	not_copied = copy_to_user(user_buffer, rand_buffer, to_copy);
	copied = to_copy - not_copied;
	printk("to copy %ld not copied %ld return %ld\n", to_copy, not_copied,
	       copied);
	*offs += copied;
	return copied;
}

/*
 * @brief Write data to buffern
 */
static ssize_t driver_write(struct file *File, const char *user_buffer,
			    size_t size, loff_t *offs)
{
	int to_copy, not_copied;
	char user_input[128] = "";
	to_copy = min(size, sizeof(user_input));

	not_copied = copy_from_user(user_input, user_buffer, to_copy);

	printk("Writing str %s\n", user_input);

	if (user_input[0] == 'm') {
		kstrtoint(&user_input[1], 10, &rand_data->min);
		printk("New min value%d\n", rand_data->min);
	} else if (user_input[0] == 'M') {
		kstrtoint(&user_input[1], 10, &rand_data->max);
		printk("New max value %d\n", rand_data->max);
	} else {
		printk("Format error %s\n", user_input);
	}

	return (to_copy - not_copied);
}

/*
 * @brief Empty open function
 */
static int driver_open(struct inode *device_file, struct file *instance)
{
	printk(DRIVER_NAME "- open was called!\n");
	return 0;
}

/*
 * @brief Empty close function
 */
static int driver_close(struct inode *device_file, struct file *instance)
{
	printk(DRIVER_NAME "- close was called!\n");
	return 0;
}

static struct file_operations module_fops = { .owner = THIS_MODULE,
					      .open = driver_open,
					      .release = driver_close,
					      .read = driver_read,
					      .write = driver_write };

/*
 * @brief Called, when the module is loaded into the kernel
 */
static int __init my_init(void)
{
	printk(DRIVER_NAME "Init!\n");

	rand_data = kmalloc(sizeof(rand_t), GFP_KERNEL);
	rand_data->max = 100;
	rand_data->min = -100;

	/* Allocate a device nr */
	if (alloc_chrdev_region(&my_device_nr, 0, 1, DRIVER_NAME) < 0) {
		printk(DRIVER_NAME "- Device Nr. could not be allocated!\n");
		return -1;
	}
	printk("read_write - Device Nr. Major: %d, Minor: %d was registered!\n",
	       my_device_nr >> 20, my_device_nr && 0xfffff);

	/* Create device class */
	if ((my_class = class_create(THIS_MODULE, DRIVER_CLASS)) == NULL) {
		printk("Device class can not be created!\n");
		goto ClassError;
	}

	/* create device file */
	if (device_create(my_class, NULL, my_device_nr, NULL, DRIVER_NAME) ==
	    NULL) {
		printk("Can not create device file!\n");
		goto FileError;
	}

	/* Initialize device file */
	cdev_init(&my_device, &module_fops);

	/* Regisering device to kernel */
	if (cdev_add(&my_device, my_device_nr, 1) == -1) {
		printk("Registering of device to kernel failed!\n");
		goto AddError;
	}

	return 0;

AddError:
	device_destroy(my_class, my_device_nr);
FileError:
	class_destroy(my_class);
ClassError:
	unregister_chrdev_region(my_device_nr, 1);
	return -1;
}

/*
 * @brief Called, when the module is removed from the kernel
 */
static void __exit my_exit(void)
{
	cdev_del(&my_device);
	device_destroy(my_class, my_device_nr);
	class_destroy(my_class);
	unregister_chrdev_region(my_device_nr, 1);
	kfree(rand_data);
	printk("Module removed\n");
}

module_init(my_init);
module_exit(my_exit);
